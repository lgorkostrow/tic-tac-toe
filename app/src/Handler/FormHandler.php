<?php

namespace App\Handler;

use App\Formatter\ApiErrorFormatter;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class FormHandler
{
    private $formFactory;
    private $errorFormatter;

    public function __construct(FormFactoryInterface $formFactory, ApiErrorFormatter $errorFormatter)
    {
        $this->formFactory = $formFactory;
        $this->errorFormatter = $errorFormatter;
    }

    public function handleFromRequest(Request $request, string $type, $entity, array $options = [])
    {
        $form = $this->formFactory->create($type, $entity, $options);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            return $form->getData();
        }

        return $form;
    }

    public function handleWithSubmit(array $data, string $type, $entity, array $options = [])
    {
        $form = $this->formFactory->create($type, $entity, $options);
        $form->submit($data);

        return $this->processSubmitted($form, $entity);
    }

    private function processSubmitted(FormInterface $form, $entity)
    {
        if (!$form->isValid()) {
            return $this->errorFormatter->format($form);
        }

        if (!is_object($entity)) {
            $entity = $form->getData();
        }

        return $entity;
    }
}