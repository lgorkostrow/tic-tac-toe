<?php

namespace App\EventListener;

use App\Exception\GameOverException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class GameOrderExceptionListener
{
    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getException();
        if (!$exception instanceof GameOverException) {
            return;
        }

        $data = [
            'form' => [
                'game' => $exception->getMessage(),
            ],
            'errors' => true,
        ];

        $event->setResponse(
            new JsonResponse($data, $exception->getCode())
        );
    }
}