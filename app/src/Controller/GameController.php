<?php

namespace App\Controller;

use App\DTO\InitGameDTO;
use App\Entity\Game;
use App\Form\InitGameDTOType;
use App\Handler\FormHandler;
use App\Service\GameService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class GameController extends AbstractController
{
    private $gameService;
    private $formHandler;

    public function __construct(GameService $gameService, FormHandler $formHandler)
    {
        $this->gameService = $gameService;
        $this->formHandler = $formHandler;
    }

    /**
     * @Route("/", methods={"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function initGame(Request $request)
    {
        $handledData = $this->formHandler->handleFromRequest($request, InitGameDTOType::class, new InitGameDTO());
        if ($handledData instanceof InitGameDTO) {
            $game = $this->gameService->init($handledData);

            return $this->redirectToRoute('app_game_startgame', ['game' => $game->getId()]);
        }

        return $this->render('pages/main.html.twig', [
            'form' => $handledData->createView(),
        ]);
    }

    /**
     * @Route("/game/{game}", methods={"GET"})
     * @param Game $game
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function startGame(Game $game)
    {
        return $this->render('pages/game.html.twig', [
            'game' => $game,
        ]);
    }
}