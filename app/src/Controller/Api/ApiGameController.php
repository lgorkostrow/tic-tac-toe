<?php

namespace App\Controller\Api;

use App\DTO\MoveGameDTO;
use App\Entity\Game;
use App\Form\MoveGameDTOType;
use App\Handler\FormHandler;
use App\Service\GameService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;

class ApiGameController extends AbstractFOSRestController
{
    private $formHandler;
    private $gameService;

    public function __construct(FormHandler $formHandler, GameService $gameService)
    {
        $this->formHandler = $formHandler;
        $this->gameService = $gameService;
    }

    /**
     * @Rest\Get("/api/game/{game}")
     * @param Game $game
     *
     * @return Game
     * @Rest\View()
     */
    public function getGame(Game $game)
    {
        return $game;
    }

    /**
     * @Rest\Post("/api/game/{game}/move")
     * @param Request $request
     * @param Game $game
     *
     * @return Game|\FOS\RestBundle\View\View
     * @Rest\View()
     */
    public function postMove(Request $request, Game $game)
    {
        $handledData = $this->formHandler->handleWithSubmit($request->request->all(), MoveGameDTOType::class, new MoveGameDTO());
        if (!$handledData instanceof MoveGameDTO) {
            return $this->view($handledData, 422);
        }

        return $this->gameService->move($game, $handledData);
    }
}