<?php

namespace App\Entity;

use App\Enum\MarkerEnum;

class Player
{
    private $name;
    private $marker;

    private function __construct(string $name, string $marker)
    {
        if (!in_array($marker, MarkerEnum::ALLOWED_MARKERS)) {
            throw new \Exception(sprintf('Invalid marker %s', $marker));
        }

        $this->name = $name;
        $this->marker = $marker;
    }

    public static function create(string $name, string $marker): self
    {
        return new self($name, $marker);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getMarker(): string
    {
        return $this->marker;
    }
}