<?php

namespace App\Entity;

class Board
{
    const BORD_SIZE = 3;

    private $data = [];
    private $countEmptyCells = self::BORD_SIZE * self::BORD_SIZE;
    private $winningComboList = [
        [
            ['x' => 1, 'y' => 1],
            ['x' => 1, 'y' => 2],
            ['x' => 1, 'y' => 3]
        ],
        [
            ['x' => 2, 'y' => 1],
            ['x' => 2, 'y' => 2],
            ['x' => 2, 'y' => 3]
        ],
        [
            ['x' => 3, 'y' => 1],
            ['x' => 3, 'y' => 2],
            ['x' => 3, 'y' => 3]
        ],
        [
            ['x' => 1, 'y' => 1],
            ['x' => 2, 'y' => 1],
            ['x' => 3, 'y' => 1]
        ],
        [
            ['x' => 1, 'y' => 2],
            ['x' => 2, 'y' => 2],
            ['x' => 3, 'y' => 2]
        ],
        [
            ['x' => 1, 'y' => 3],
            ['x' => 2, 'y' => 3],
            ['x' => 3, 'y' => 3]
        ],
        [
            ['x' => 1, 'y' => 1],
            ['x' => 2, 'y' => 2],
            ['x' => 3, 'y' => 3]
        ],
        [
            ['x' => 3, 'y' => 1],
            ['x' => 2, 'y' => 2],
            ['x' => 1, 'y' => 3]
        ],
    ];

    public function __construct()
    {
        for ($i = 1; $i <= self::BORD_SIZE; $i++) {
            $this->data[$i] = array_fill(1, self::BORD_SIZE, null);
        }
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function changeCellValue(int $x, int $y, string $value): self
    {
        if (!empty($this->data[$y][$x])) {
            throw new \Exception('Forbidden');
        }

        $this->data[$y][$x] = $value;

        $this->countEmptyCells--;

        return $this;
    }

    public function checkComboForMarker(string $marker): bool
    {
        foreach ($this->winningComboList as $item) {
            $count = 0;
            foreach ($item as $coordinates) {
                $cell = $this->data[$coordinates['y']][$coordinates['x']];
                if ($cell !== $marker) {
                    break;
                }

                $count++;
            }

            if (self::BORD_SIZE === $count) {
                return true;
            }
        }

        return false;
    }

    public function isFull(): bool
    {
        return 0 === $this->countEmptyCells;
    }
}