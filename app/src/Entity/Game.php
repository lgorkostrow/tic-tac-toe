<?php

namespace App\Entity;

use App\Exception\GameOverException;
use App\State\FinishState;
use App\State\InProgressState;
use App\State\NewState;
use App\State\State;

class Game
{
    private $id;
    private $playerOne;
    private $playerTwo;
    private $board;
    private $state;
    private $winner;
    private $currentPlayer;

    private function __construct(Player $playerOne, Player $playerTwo)
    {
        $this->playerOne = $this->currentPlayer = $playerOne;
        $this->playerTwo = $playerTwo;
        $this->board = new Board();
        $this->state = new NewState();
    }

    public static function create(Player $playerOne, Player $playerTwo): self
    {
        return new self($playerOne, $playerTwo);
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getPlayerOne(): Player
    {
        return $this->playerOne;
    }

    public function getPlayerTwo(): Player
    {
        return $this->playerTwo;
    }

    public function getBoard(): Board
    {
        return $this->board;
    }

    public function getWinner(): ?Player
    {
        return $this->winner;
    }

    public function start(): self
    {
        $this->changeState(new InProgressState());

        return $this;
    }

    public function finish(): self
    {
        $this->changeState(new FinishState());

        return $this;
    }

    public function move(int $x, int $y): self
    {
        if (!$this->state->allowsModification()) {
            throw new GameOverException(400);
        }

        $this->board->changeCellValue($x, $y, $this->currentPlayer->getMarker());

        if ($this->board->checkComboForMarker($this->currentPlayer->getMarker())) {
            $this->winner = $this->currentPlayer;
            $this->finish();

            return $this;
        }

        if ($this->board->isFull()) {
            $this->finish();

            return $this;
        }

        $this->currentPlayer = $this->currentPlayer == $this->playerOne ? $this->playerTwo : $this->playerOne;

        return $this;
    }

    public function isStarted(): bool
    {
        return $this->state instanceof InProgressState;
    }

    public function isFinished(): bool
    {
        return $this->state instanceof FinishState;
    }

    private function changeState(State $state)
    {
        if (!$this->state->canBeChangedTo($state)) {
            throw new \Exception(sprintf('Invalid state %s', $state->getName()));
        }

        $this->state = $state;
    }
}