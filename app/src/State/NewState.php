<?php

namespace App\State;

class NewState extends State
{
    const NAME = 'new';
    
    protected $name = self::NAME;
    protected $next = [
        InProgressState::class,
    ];
}