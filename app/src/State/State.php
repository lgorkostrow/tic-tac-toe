<?php

namespace App\State;

abstract class State
{
    protected $name = '';
    protected $next = [];

    public function canBeChangedTo(State $state): bool
    {
        return in_array(get_class($state), $this->next, true);
    }

    public function allowsModification(): bool
    {
        return false;
    }

    public function getName(): string
    {
        return $this->name;
    }
}