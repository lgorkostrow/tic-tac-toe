<?php

namespace App\State;

class FinishState extends State
{
    const NAME = 'finish';

    protected $name = self::NAME;
}