<?php

namespace App\State;

class InProgressState extends State
{
    const NAME = 'in_progress';

    protected $name = self::NAME;
    protected $next = [
        FinishState::class,
    ];

    public function allowsModification(): bool
    {
        return true;
    }
}