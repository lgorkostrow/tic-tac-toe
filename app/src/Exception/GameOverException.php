<?php

namespace App\Exception;

use Throwable;

class GameOverException extends \Exception
{
    const MESSAGE = 'Game over. Thank you for playing';

    public function __construct($code = 0, Throwable $previous = null)
    {
        parent::__construct(self::MESSAGE, $code, $previous);
    }
}