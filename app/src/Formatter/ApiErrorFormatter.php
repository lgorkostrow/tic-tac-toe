<?php

namespace App\Formatter;

use Symfony\Component\Form\FormInterface;

class ApiErrorFormatter
{
    public function format(FormInterface $form)
    {
        return [
            'form' => $this->serializeFormErrors($form),
            'errors' => true,
        ];
    }

    private function serializeFormErrors(FormInterface $form)
    {
        $errors = [
            'children' => [],
            'errors' => [],
        ];

        foreach ($form->getErrors() as $error) {
            $message = $error->getMessage();
            preg_match('/\`(.+)\`:(.+)/', $message, $matches);

            if (is_array($matches) && array_key_exists(1, $matches)) {
                $errors['children'][$matches[1]] = trim($matches[2]);
            } else {
                $errors['children'][] = $message;
            }
        }

        $errors['children'] = array_merge(
            $errors['children'],
            $this->serialize($form)
        );

        return $errors;
    }

    private function serialize(FormInterface $form)
    {
        $localErrors = [];
        if (count($form->getIterator()) <= 0) {
            return;
        }

        foreach ($form->getIterator() as $key => $child) {
            foreach ($child->getErrors() as $error) {
                $localErrors[$key] = $error->getMessage();
            }

            if (count($child->getIterator()) === 0) {
                continue;
            }

            $serializedChild = $this->serialize($child);
            if (empty($serializedChild)) {
                continue;
            }

            $localErrors[$key] = $serializedChild;
        }

        return $localErrors;
    }
}
