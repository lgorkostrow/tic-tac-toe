<?php

namespace App\Enum;

class MarkerEnum
{
    const MARKER_X = 'X';
    const MARKER_O = 'O';

    const ALLOWED_MARKERS = [
        self::MARKER_X,
        self::MARKER_O,
    ];
}