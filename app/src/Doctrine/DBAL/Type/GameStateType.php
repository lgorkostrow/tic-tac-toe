<?php

namespace App\Doctrine\DBAL\Type;

use App\State\FinishState;
use App\State\InProgressState;
use App\State\NewState;
use App\State\State;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class GameStateType extends Type
{
    const NAME = 'gamestate';

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return sprintf(
            "ENUM('%s', '%s', '%s') COMMENT '(DC2Type: %s)'",
            NewState::NAME,
            InProgressState::NAME,
            FinishState::NAME,
            self::NAME
        );
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        switch ($value) {
            case NewState::NAME:
                return new NewState();
            case InProgressState::NAME:
                return new InProgressState();
            case FinishState::NAME:
                return new FinishState();
        }
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (!$value instanceof State) {
            throw new \InvalidArgumentException("Invalid state");
        }

        return $value->getName();
    }

    public function getName()
    {
        return self::NAME;
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }
}
