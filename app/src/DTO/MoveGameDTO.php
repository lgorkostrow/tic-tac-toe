<?php

namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class MoveGameDTO
{
    /**
     * @Assert\NotBlank
     * @Assert\Range(min="1", max=App\Entity\Board::BORD_SIZE)
     */
    public $x;

    /**
     * @Assert\NotBlank
     * @Assert\Range(min="1", max=App\Entity\Board::BORD_SIZE)
     */
    public $y;
}