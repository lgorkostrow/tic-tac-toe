<?php

namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class InitGameDTO
{
    /**
     * @Assert\NotBlank
     * @Assert\Length(max="255")
     */
    public $playerOneName;

    /**
     * @Assert\NotBlank
     * @Assert\Length(max="255")
     */
    public $playerTwoName;
}