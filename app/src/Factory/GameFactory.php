<?php

namespace App\Factory;

use App\Entity\Game;
use App\Entity\Player;

class GameFactory
{
    public function create(Player $playerOne, Player $playerTwo): Game
    {
        return Game::create(
            $playerOne,
            $playerTwo
        );
    }
}