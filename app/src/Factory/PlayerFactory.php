<?php

namespace App\Factory;

use App\Entity\Player;

class PlayerFactory
{
    public function create(string $playerName, string $marker): Player
    {
        return Player::create($playerName, $marker);
    }
}