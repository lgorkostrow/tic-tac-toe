<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190725171321 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE game (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', state ENUM(\'new\', \'in_progress\', \'finish\') COMMENT \'(DC2Type: gamestate)\' NOT NULL COMMENT \'(DC2Type:gamestate)\', board_data JSON NOT NULL, board_count_empty_cells INT NOT NULL, player_one_name VARCHAR(255) DEFAULT NULL, player_one_marker VARCHAR(10) DEFAULT NULL, player_two_name VARCHAR(255) DEFAULT NULL, player_two_marker VARCHAR(10) DEFAULT NULL, winner_name VARCHAR(255) DEFAULT NULL, winner_marker VARCHAR(10) DEFAULT NULL, current_player_name VARCHAR(255) DEFAULT NULL, current_player_marker VARCHAR(10) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE game');
    }
}
