<?php

namespace App\Service;

use App\DTO\InitGameDTO;
use App\DTO\MoveGameDTO;
use App\Entity\Game;
use App\Enum\MarkerEnum;
use App\Factory\GameFactory;
use App\Factory\PlayerFactory;
use App\Repository\GameRepository;

class GameService
{
    private $playerFactory;
    private $gameFactory;
    private $gameRepository;

    public function __construct(
        PlayerFactory $playerFactory,
        GameFactory $gameFactory,
        GameRepository $gameRepository
    )
    {
        $this->playerFactory = $playerFactory;
        $this->gameFactory = $gameFactory;
        $this->gameRepository = $gameRepository;
    }

    public function init(InitGameDTO $initGameDTO): Game
    {
        $playerOne = $this->playerFactory->create($initGameDTO->playerOneName, MarkerEnum::MARKER_X);
        $playerTwo = $this->playerFactory->create($initGameDTO->playerTwoName, MarkerEnum::MARKER_O);

        $game = $this->gameFactory->create(
            $playerOne,
            $playerTwo
        );

        $game->start();

        $this->gameRepository->save($game);

        return $game;
    }

    public function move(Game $game, MoveGameDTO $moveGameDTO): Game
    {
        $game->move(
            $moveGameDTO->x,
            $moveGameDTO->y
        );

        $this->gameRepository->save($game);

        return $game;
    }
}