import axios from 'axios'

const baseUrl = '/api';

const instance = axios.create({
   baseURL: baseUrl,
});

export const getGame = gameId => {
    const getGameUrl = `/game/${gameId}`;

    return instance.get(getGameUrl).then(data => data);
};

export const makeMove = (gameId, params) => {
    const makeMoveUrl = `/game/${gameId}/move`;

    return instance.post(makeMoveUrl, params).then(data => data);
};

export default instance;