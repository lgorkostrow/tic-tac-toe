/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require('../css/app.css');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
// const $ = require('jquery');
import 'bootstrap/dist/css/bootstrap.min.css';
import Vue from 'vue';
import Game from './components/Game';

import SweetModal from 'sweet-modal-vue/src/plugin'
Vue.use(SweetModal);

new Vue({
    el: '#app',
    components: {Game}
}).$mount('#app');
